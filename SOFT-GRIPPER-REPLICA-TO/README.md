##### This is a replica of the soft-gripper shown in this publication : [Grasping with kirigami shells](https://www.researchgate.net/figure/Demonstration-of-a-kirigami-shell-as-a-soft-gripper_fig1_351538904)

---

##### You can check this video to see how the gripper works : https://youtu.be/kDK5UjnRDsw)


| SOFT-GRIPPER |
| - |
| ![soft-gripper-replica](SOFT-GRIPPER-REPLICA-TO/SOFT-GRIPPER-LASER-CUT.png)|
